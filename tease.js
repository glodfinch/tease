import { getSetting, updateSetting } from './lib/settings.js'

import awazon from './shops/Awazon.js'
import bitterleaf from './shops/Bitterleaf.js'
import chaWangShop from './shops/ChaWangShop.js'
import chenShengHao from './shops/ChenShengHao.js'
import essenceOfTea from './shops/EssenceOfTea.js'
import farmerLeaf from './shops/FarmerLeaf.js'
import fullchea from './shops/Fullchea.js'
import kingTeaMall from './shops/KingTeaMall.js'
import liquidProust from './shops/LiquidProust.js'
import purpleCloud from './shops/PurpleCloud.js'
import theTea from './shops/TheTea.js'
import white2Tea from './shops/White2Tea.js'
import wuyiOrigin from './shops/WuyiOrigin.js'
import yeeOnTea from './shops/YeeOnTea.js'
import yunnanSourcing from './shops/YunnanSourcing.js'
import whatCha from './shops/WhatCha.js'
import westChinaTea from './shops/WestChinaTea.js'
import vietSun from './shops/VietSun.js'
import yunomi from './shops/Yunomi.js'

const hostFuncs = {
  'pu-erhtea.com': awazon,
  'bitterleafteas.com': bitterleaf,
  'chawangshop.com': chaWangShop,
  'cspuerh.com': chenShengHao,
  'essenceoftea.com': essenceOfTea,
  'farmer-leaf.com': farmerLeaf,
  'fullchea-tea.com': fullchea,
  'kingteamall.com': kingTeaMall,
  'liquidproust.com': liquidProust,
  'white2tea.com': white2Tea,
  'wuyiorigin.com': wuyiOrigin,
  'yeeonteaco.com': yeeOnTea,
  'westchinatea.com': westChinaTea,
  'what-cha.com': whatCha,
  'yunnansourcing.com': yunnanSourcing,
  'yunnansourcing.us': yunnanSourcing,
  'thetea.pl': theTea,
  'purplecloudteahouse.com': purpleCloud,
  'vietsuntea.com': vietSun,
  'yunomi.life': yunomi
}

const settingsDefaults = {
  decimalPlaces: 2,
  secondaryWeight: 357,
  ktmText: false
}

const main = async () => {
  if (window.teaseHasRun) return
  window.teaseHasRun = true
  const hostname = window.location.hostname.replace('www.', '')
  console.log('tease started', hostname)

  for (const key of Object.keys(settingsDefaults)) {
    const settingValue = await getSetting(key)
    if (!settingValue) await updateSetting(key, settingsDefaults[key])
  }

  const shopFunc = hostFuncs[hostname]
  await shopFunc()
}

export default main
