export const parseShopifyData = () => {
  const shopifyScript = document.querySelector('#web-pixels-manager-setup').textContent
  const json = /.*initData: (?<json>\{.*\}),\},function.*/.exec(shopifyScript).groups?.json
  const data = JSON.parse(json)
  console.log(data.productVariants)
  return data.productVariants
}
