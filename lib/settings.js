export const updateSetting = async (key, val) => {
	await chrome.storage.local.set({ [key]: val })
}

export const getSetting = async (key) => {
	const settings = await chrome.storage.local.get()
	return settings[key]
}