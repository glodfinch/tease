import { removePrices } from '../render.js'

export const getChildText = ($el) => {
  const $textNode = [...$el.childNodes].filter(($node) => $node.nodeType === Node.TEXT_NODE).pop()
  if (!$textNode) return null
  return $textNode.textContent
}

export const startMutationObserver = ($el, updateFunc, opt = {}) => {
  const observer = new MutationObserver(async ([mutation]) => {
    const res = await updateFunc(mutation.target)
    if (res === false) removePrices()
  })

  if ($el) {
    observer.observe($el, { childList: true, ...opt })
    return observer
  }

  return null
}

export const probeNodeRemoval = ($target) => {
  console.log($target)
  var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
      var nodes = Array.from(mutation.removedNodes)
      var directMatch = nodes.indexOf($target) > -1
      var parentMatch = nodes.some((parent) => parent.contains($target))
      if (directMatch) {
        console.log('node', $target, 'was directly removed!')
      } else if (parentMatch) {
        console.log('node', $target, 'was removed through a removed parent!')
      }
    })
  })

  var config = {
    subtree: true,
    childList: true
  }

  observer.observe(document.body, config)
}
