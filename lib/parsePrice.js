const getCurrencyCode = (priceString) => {
  const lookup = {
    '£': 'GBP',
    '$': 'USD',
    '€': 'EUR',
    'zł': 'PLN',
    '¥': 'CNY',
    'kr': 'DKK',
    'Rp': 'IDR'
  }

  let currencyCode = null

  for (const key of Object.keys(lookup)) {
    if (priceString.includes(key)) {
      currencyCode = lookup[key]
      break
    }
  }

  return currencyCode
}

const isCommaDecimal = (priceString) => {
  // the only time where it can be comma separated currency is if the third to last character is a comma... right?
  return /^[\d\.]+,\d{2}$/.test(priceString)
}

export const parsePrice = (priceString) => {
  const currencyCode = getCurrencyCode(priceString)
  priceString = priceString.replace(/[^\d\.,]/g, '')

  console.log('ps', priceString, isCommaDecimal(priceString))

  if (isCommaDecimal(priceString)) {
    priceString = priceString.replace('.', '').replace(',', '.')
  } else {
    priceString = priceString.replace(',', '')
  }

  const value = parseFloat(priceString)

  return {
    value,
    currencyCode
  }
}
