const updateSetting = async (key, val) => {
  await chrome.storage.local.set({ [key]: val })
}

const getSetting = async (key) => {
  const settings = await chrome.storage.local.get()
  return settings[key]
}

const resetToDefaults = async () => {
  await updateSetting('decimalPlaces', 2)
  await updateSetting('secondaryWeight', 357)
  await updateSetting('ktmText', false)
}

document.addEventListener('DOMContentLoaded', async () => {
  document.getElementById('versionString').textContent = `v${chrome.runtime.getManifest().version}`

  const $dp = document.getElementById('dpInput')
  const $sw = document.getElementById('swInput')
  const $ktm = document.getElementById('ktmCheckbox')

  $dp.value = (await getSetting('decimalPlaces')) || 2
  $sw.value = (await getSetting('secondaryWeight')) || 357
  $ktm.checked = (await getSetting('ktmText')) || false

  $dp.addEventListener('change', async (e) => {
    const val = parseInt(e.target.value)
    await updateSetting('decimalPlaces', val)
  })

  $sw.addEventListener('change', async (e) => {
    const val = parseInt(e.target.value)
    await updateSetting('secondaryWeight', val)
  })

  $ktm.addEventListener('change', async (e) => {
    const val = $ktm.checked
    await updateSetting('ktmText', val)
  })

  document.getElementById('resetToDefaults').addEventListener('click', async () => {
    await resetToDefaults()
    $dp.value = await getSetting('decimalPlaces')
    $sw.value = await getSetting('secondaryWeight')
    $ktm.checked = await getSetting('ktmText')
  })

  document.getElementById('logo').src = chrome.runtime.getURL('icons/icon.png')
})
