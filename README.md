# Tease

Adds extra information about tea prices to various websites.

Firefox: https://addons.mozilla.org/en-US/firefox/addon/glodfinch-tease/

Chrome: https://chrome.google.com/webstore/detail/tease/dehhdenomhmljegaokkfakebaabfonbj

Works on:

- Awazon
- Bitterleaf
- ChaWangShop
- Chen Sheng Hao
- Essence of Tea
- Farmer Leaf
- Fullchea
- KingTeaMall
- Liquid Proust
- Purple Cloud Tea House
- TheTea.pl
- Viet Sun
- West China Tea
- WhatCha
- White2Tea
- Wuyi Origin
- YeeOnTea
- Yunnan Sourcing
- Yunomi.life

On Firefox, you'll need to grant permissions to access the website data. When on the website, right click the extension and always allow. Or go to the extensions section in the add-ons page and grant all the permissions. It may also be necessary to grant new permissions when the extension gets updated, from what I can tell this seems to be a problem with Firefox's Manifestv3 implementation that'll get fixed at some point and then it can handle the permissions automatically.

Weight and price data is extracted from the page with regex, so while it works on the majority of listings, occasionally it won't be able to parse the text. Feel free to raise an issue if you find any that don't work.
