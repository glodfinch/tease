import { calculatePrices, renderPrices } from '../render.js'
import { parsePrice } from '../lib/parsePrice.js'

const grams = /^(?<weight>\d+)g$/i
const kilograms = /^(?<weight>\d+)kg$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const start = async () => {
  const $cont = document.querySelector('.product-info-main')
  if (!$cont) return

  const titleString = $cont.querySelector('.page-title .base').textContent
  const weight = findGramWords(titleString)
  if (!weight) return

  const $price = $cont.querySelector('.price')
  if (!$price) return
  const { value, currencyCode } = parsePrice($price.textContent)

  const prices = await calculatePrices(value, weight)
  renderPrices($cont.querySelector('.price-container'), prices, currencyCode, 'font-size: 14px;')
}

export default start
