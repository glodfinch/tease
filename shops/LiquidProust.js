import { parsePrice } from '../lib/parsePrice.js'
import { calculatePrices, renderPrices } from '../render.js'

const parseTitleString = (string) => {
  const split = string.split(' ')
  const gramWords = split.map((word) => /^\(?\~?(?<weight>\d+)g\+?\)?$/i.exec(word.trim())?.groups).filter((el) => !!el)

  if (gramWords.length === 0 || gramWords.length > 1) return null
  return parseFloat(gramWords.pop().weight)
}

const start = async () => {
  const cont = document.querySelector('.listing-purchase-box')
  if (!cont) return

  const titleString = cont.querySelector('.listing-title').textContent
  const titleGrams = parseTitleString(titleString)
  if (!titleGrams) return

  const priceString = cont.querySelector('span[data-ui="base-price"]').textContent
  const { value, currencyCode } = parsePrice(priceString)

  const prices = await calculatePrices(value, titleGrams)
  const renderEl = cont.querySelector('.listing-price')
  renderPrices(renderEl, prices, currencyCode, 'font-size: 12px;')
}

export default start
