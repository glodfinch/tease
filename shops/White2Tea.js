import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^~?(?<weight>\d+)g$/
const kilograms = /^(?<weight>\d+)kg$/

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const selectedWeightEl = document.querySelector('.option-selector__btns input:checked')
  const weightValue = selectedWeightEl ? selectedWeightEl.getAttribute('value') : products[0].title
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()
  const selectedWeight = parseWeightString(weightValue)
  if (!selectedWeight) return false

  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode

  const prices = await calculatePrices(listPrice, selectedWeight)
  renderPrices(document.querySelector('.price-container .price__default'), prices, currencyCode, 'flex-basis: 100%;')
}

const start = () => {
  const $priceContainer = document.querySelector('.price .price__current')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
