import { calculatePrices, renderPrices } from '../render.js'
import { parsePrice } from '../lib/parsePrice.js'
import { probeNodeRemoval, startMutationObserver } from '../lib/utils.js'

let priceObserver = null
const grams = /^(?<weight>\d+)g$/i
const kilograms = /^(?<weight>\d+)kg$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  // wait for whatever dumb dom stuff they're doing to finish so we get the right price
  await new Promise((resolve) => setTimeout(resolve, 50))

  const $selectedWeight = document.querySelector(
    'div[data-hook="product-options-inputs"] div[data-hook="dropdown-base-text"]'
  )

  if (!$selectedWeight) return false
  const weightValue = findGramWords($selectedWeight.textContent)
  if (!weightValue) return false

  const $price = document.querySelector('span[data-hook="formatted-primary-price"]:first-child')
  const priceText = $price.textContent
  const { value, currencyCode } = parsePrice(priceText)
  const prices = await calculatePrices(value, weightValue)

  renderPrices(
    document.querySelector('div[data-hook="product-prices-wrapper"]'),
    prices,
    currencyCode,
    'color: rgb(87, 87, 87)'
  )
}

// this setup is necessary because viet sun do some react and client side routing stuff
// so it deletes and recreates a ton of nodes all the time and it's a pain in the arse
// maybe it would be simpler to just listen for clicks...
const startPriceObserver = () => {
  const $priceContainer = document.querySelector('div[data-hook="product-price"]')

  if (priceObserver) {
    priceObserver.disconnect()
    priceObserver.observe($priceContainer, { childList: true, subtree: true })
  } else {
    priceObserver = startMutationObserver($priceContainer, updatePrice, { subtree: true })
  }

  updatePrice()
}

const start = async () => {
  new MutationObserver((mutations, observer) => {
    // prevent a loop when we render tease
    if (mutations.length === 1) {
      const $teaseMain = document.getElementById('teaseMain')

      if ($teaseMain) {
        const { target } = mutations[0]
        if ($teaseMain.contains(target)) return
      }
    }

    const $priceContainer = document.querySelector('div[data-hook="product-price"]')
    if ($priceContainer) startPriceObserver()
  }).observe(document.querySelector('#SITE_CONTAINER'), { childList: true, subtree: true })
}

export default start
