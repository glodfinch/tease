import { calculatePrices, renderPrices } from '../render.js'
import { parsePrice } from '../lib/parsePrice.js'

const justGrams = /^(?<weight>\d+)(?: ?g.*)?(?: ?grams.*)?$/i
const justKilos = /^(?<weight>\d+\.?\d*) ?kg.*$/i
const cakeWithWeight =
  /^(?<qty>\d+) (?:cake|piece|bag|cake tea sample|brick sample|cake sample) \((?<weight>\d+) ?g(?:rams)?\)$/i
const justCake = /^(?<qty>\d+) (?:cakes?|box|brick).*$/i
const tong = /^(?<mul>\d+) tong \((?<qty>\d+) cakes?.*\)$/i
const pack = /^\d+(?: ?pack| ?bag| ?kg| ?tong)(?: ?\+ ?1 ?cake)? ?\((?<weight>\d+)g x (?<qty>\d+) (?:cakes|pcs).*$/i
const piece = /^(?<qty>\d+) pieces? \((?<weight>\d+) grams\)$/i

const parseTitleString = (string) => {
  const split = string.split(' ')
  const gramWords = split.filter((word) => word.match(/^[0-9]+g(?:\*.*)?(?:,)?$/))
  if (gramWords.length === 0 || gramWords.length > 1) return null
  return parseFloat(gramWords.pop())
}

const parseWeightString = (string, cakeWeight) => {
  string = string.trim()
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  match = justKilos.exec(string)?.groups
  if (match) return match.weight * 1000
  match = cakeWithWeight.exec(string)?.groups
  if (match) return match.qty * match.weight
  match = pack.exec(string)?.groups
  if (match) return match.qty * match.weight
  match = piece.exec(string)?.groups
  if (match) return match.qty * match.weight

  if (!cakeWeight) return null
  match = justCake.exec(string)?.groups
  if (match) return match.qty * cakeWeight
  match = tong.exec(string)?.groups
  if (match) return match.mul * match.qty * cakeWeight
  return null
}

const start = async () => {
  const $titleString = document.querySelector('#ctl00_Contents_LbTeaName').textContent
  const titleGrams = parseTitleString($titleString)

  const $selectedWeight = document.querySelector('#ctl00_Contents_DdlSamples option[selected]')
  const weightString = $selectedWeight.textContent
  const selectedWeight = parseWeightString(weightString, titleGrams)
  if (!selectedWeight) return

  const $price = document.getElementById('ctl00_Contents_LbPrice')
  if (!$price) return
  const { value, currencyCode } = parsePrice($price.textContent)
  const prices = await calculatePrices(value, selectedWeight)

  renderPrices(
    document.querySelector('#ctl00_Contents_LbPrice').parentNode,
    prices,
    currencyCode,
    'color: red; margin-bottom: 10px; justify-content: flex-end; padding-right: 140px;'
  )
}

export default start
