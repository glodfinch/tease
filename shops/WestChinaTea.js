import { parsePrice } from '../lib/parsePrice.js'
import { calculatePrices, renderPrices } from '../render.js'

const weight = /^(?<value>\d+\.\d+)\s*(?<weight>\w+)?$/

const parseWeightString = (string) => {
  let match = weight.exec(string)?.groups
  if (match) return match
  return null
}

const start = async () => {
  const weightString = document.querySelector('dd[data-product-weight]').textContent
  const selectedWeight = parseWeightString(weightString)
  if (!selectedWeight) return

  const priceString = document.querySelector('.price--withoutTax').textContent
  const { value } = parsePrice(priceString)

  const prices = await calculatePrices(value, selectedWeight.value)
  const currencyCode = document.querySelector('main[data-currency-code]').dataset.currencyCode

  renderPrices(
    document.querySelector('.price-section--withoutTax:not([style*="display: none"])'),
    prices,
    currencyCode,
    'font-size: 12px;'
  )
}

export default start
