import { calculatePrices, renderPrices } from '../render.js'
import { parsePrice } from '../lib/parsePrice.js'
import { startMutationObserver } from '../lib/utils.js'

const justGrams = /^(?<weight>\d+)g$/i
const gramsWithQty = /^(?<weight>\d+)g \* (?<qty>\d+)$/i

const parseTitleString = (string) => {
  const split = string.split(' ')
  const gramWords = split.filter((word) => word.match(/^[0-9]+g(?:\*.*)?(?:,)?$/))
  if (gramWords.length === 0 || gramWords.length > 1) return null
  return parseFloat(gramWords.pop())
}

const parseWeightString = (string, cakeWeight) => {
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  match = gramsWithQty.exec(string)?.groups
  if (match) return match.weight * match.qty
  return null
}

const start = async () => {
  const cont = document.querySelector('.detail_right')
  if (!cont) return
  const titleString = cont.querySelector('.themes_products_title').textContent
  const titleGrams = parseTitleString(titleString)

  const updatePrice = async (el) => {
    const weightSelectorCont = cont.querySelector('li[name="Weight"]')

    const selectedWeight = weightSelectorCont
      ? (() => {
          const weightButton = weightSelectorCont.querySelector('.btn_attr.selected')
          if (weightButton) return parseWeightString(weightButton.textContent.trim())
          return titleGrams
        })()
      : titleGrams

    if (!selectedWeight) return false

    const priceString = el.textContent
    const { value, currencyCode } = parsePrice(priceString)
    const renderEl = cont.querySelector('.current_price')
    const prices = await calculatePrices(value, selectedWeight)

    renderPrices(
      renderEl,
      prices,
      currencyCode,
      'clear: left; font-size: 12px; color: #999;',
      'color: #c00; font-weight: semi-bold'
    )
  }

  const $priceContainer = cont.querySelector('#cur_price')
  startMutationObserver($priceContainer, () => updatePrice($priceContainer))
  updatePrice($priceContainer)
}

export default start
