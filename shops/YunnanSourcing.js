import { calculatePrices, removePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { getChildText, startMutationObserver } from '../lib/utils.js'

const price = /^(?<symbol>\D)?(?<value>\d+\.\d+)(?: \w*)?$/i
const justGrams = /^(?<weight>\d+) grams?(?: sample| cake| brick| tuo)?.*$/i
const justKilos = /^(?<weight>\d+) kilograms?.*$/i
const cakeGrams = /^(?<qty>\d+) (?:cake|basket|box|tuo|tin|brick|case) \((?:roughly )?(?<weight>\d+) grams?\).*$/i
const cakeKilos = /^(?<qty>\d+) (?:cake|basket|box|tuo|tin|brick|case) \((?<weight>\d+) kilograms?\)$/i
const tongGrams = /^(?<mul>\d+) (?:bag|tong) \((?<qty>\d+) [x\*] (?<weight>\d+) grams?\)$/i
const tongKilos = /^(?<mul>\d+) (?:bag|tong) \((?<qty>\d+) x (?<weight>\d+) kilograms?\)$/i

const parseWeightString = (string) => {
  string = string.trim()
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  match = justKilos.exec(string)?.groups
  if (match) return match.weight * 1000
  match = cakeGrams.exec(string)?.groups
  if (match) return match.qty * match.weight
  match = cakeKilos.exec(string)?.groups
  if (match) return match.qty * match.weight * 1000
  match = tongGrams.exec(string)?.groups
  if (match) return match.mul * match.qty * match.weight
  match = tongKilos.exec(string)?.groups
  if (match) return match.mul * match.qty * match.weight * 1000
  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const $optionsSelectors = [
    ...document.querySelectorAll('.product-details .variant-selection .options-selection__select')
  ]

  const $weightOption = $optionsSelectors
    .filter(($el) => ['Weight', 'Quantity'].includes($el.querySelector('label').textContent))
    .pop()
  if (!$weightOption) return false
  const $harvestOption = $optionsSelectors
    .filter(($el) => ['Harvest & Year', 'Harvest & Season', 'Harvest'].includes($el.querySelector('label').textContent))
    .pop()
  const weightString = $weightOption.querySelector('select').getAttribute('data-variant-option-chosen-value')
  const harvestString = $harvestOption
    ? $harvestOption.querySelector('select').getAttribute('data-variant-option-chosen-value')
    : null
  const titleString = harvestString ? `${weightString} / ${harvestString}` : weightString

  const selectedWeight = parseWeightString(weightString)
  if (!selectedWeight) return false

  const selectedProduct = products.filter((p) => p.title.toLowerCase() === titleString.toLowerCase()).pop()
  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)
  renderPrices(document.querySelector('.product-details .product__price'), prices, currencyCode, 'font-size: 12px;')
}

const handleCart = async () => {
  const $cartItems = [...document.querySelectorAll('.cart-item')]

  let totalWeight = 0

  for (const $item of $cartItems) {
    const $optionEls = [...$item.querySelectorAll('.cart-item .cart-item--option-name')]

    for (const $el of $optionEls) {
      console.log($el.textContent)
      if ($el.textContent === 'Weight') {
        console.log('!!')
        const weightString = getChildText($el.parentElement)
        console.log(weightString)
        const weightParsed = parseWeightString(weightString)
        console.log(weightParsed)
      }
    }
  }
}

const start = async () => {
  console.log('start', window.location.pathname)

  if (/^\/?.*\/cart$/i.test(window.location.pathname)) {
    //console.log('handleCart')
    //handleCart()
  } else {
    const $priceContainer = document.querySelector('.product-details .price__current')
    startMutationObserver($priceContainer, updatePrice)
    updatePrice()
  }
}

export default start
