import { getSetting } from '../lib/settings.js'
import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const justGrams = /^(?:sample )?(?<weight>\d+)g ?.*$/i
const individual = /^(?:\d{4}yr\. )?individual ?(?:cake|brick|tuo)?.*$/
const tong = /^(?:stack |tong )?\(?(?<qty>\d+)(?:pcs?| tuo| cakes| bricks| box| boxes).*\)?$/

const parseTitleString = (string) => {
  const split = string.split(' ')
  const gramWords = split.filter((word) => word.match(/^[0-9]+g(?:\*.*)?(?:,)?$/))
  if (gramWords.length === 0 || gramWords.length > 1) return null
  return parseFloat(gramWords.pop())
}

const parseWeightString = (string, cakeWeight) => {
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  if (!cakeWeight) return null
  match = tong.exec(string)?.groups
  if (match) return match.qty * cakeWeight
  if (string.match(individual)) return cakeWeight
  return null
}

const updatePrice = async () => {
  console.log('updatePrice')
  const products = parseShopifyData()
  const cont = document.querySelector('.product-single__meta')
  if (!cont) return false
  const selector = cont.querySelector('#SingleOptionSelector-0')
  const titleString = cont.querySelector('.product-single__title').textContent
  const titleGrams = parseTitleString(titleString)
  const weightString = selector.value.toLowerCase()
  const selectedWeight = parseWeightString(weightString, titleGrams)
  if (!selectedWeight) return false
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightString.toLowerCase()).pop()
  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)

  if (selectedWeight) {
    renderPrices(
      cont.querySelector('.price'),
      prices,
      currencyCode,
      'flex-basis: 100%; font-size: 0.6em;',
      'color: #69727b; font-weight: 600;'
    )
  }
}

const start = async () => {
  const ktmText = await getSetting('ktmText')

  if (ktmText) {
    document.getElementById('shopify-section-hero_FHYgDV')?.remove()
    document.getElementById('shopify-section-hero_8YyhGq')?.remove()
    document.getElementById('shopify-section-73e56e35-d23f-4b09-aa04-fe8df5a32045')?.remove()
    document
      .querySelector('#shopify-section-collection-template > div > header > div.page-width:nth-child(1) .rte')
      ?.remove()
  }

  const $priceContainer = document.querySelector('.product-single__meta .price__regular')
  startMutationObserver($priceContainer, updatePrice, { subtree: true })
  updatePrice()
}

export default start
