import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^\(?~?(?<weight>\d+(\.\d+)?)g$/
const kilograms = /^(?<weight>\d+)kg$/

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const $selectedWeightEl = document.querySelector('.omega .swatch_options input:checked')
  const weightValue = $selectedWeightEl.getAttribute('value')
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()
  const selectedWeight = findGramWords(weightValue)
  if (!selectedWeight) return false

  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)
  renderPrices(document.querySelector('.omega .modal_price'), prices, currencyCode, 'justify-content: center')
}

const start = () => {
  const $priceContainer = document.querySelector('.omega .current_price')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
