import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^(?<weight>\d+)g.*$/i
const kilograms = /^(?<weight>\d+)kg.*$/i
const tong = /^Tong \((?<qty>\d+) cakes \/ (?<weight>\d+)kg\)$/i
const multipleTins = /^(?<weight>\d+)g Tin x (?<qty>\d+)$/i

const parseWeightString = (string) => {
  let match = tong.exec(string)?.groups
  if (match) return match.weight * 1000
  match = multipleTins.exec(string)?.groups
  if (match) return match.qty * match.weight
  match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const $weightSelect = document.getElementById('ProductSelect-product-template-option-0')
  const weightValue = $weightSelect.value
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()
  let selectedWeight = parseWeightString(weightValue)

  if (!selectedWeight && products.length === 1) {
    const titleString = document.querySelector('.product-single__title').textContent
    const titleGrams = findGramWords(titleString)
    selectedWeight = titleGrams
  }

  if (!selectedWeight) return false

  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode

  const prices = await calculatePrices(listPrice, selectedWeight)
  renderPrices(document.querySelector('.product-single__prices'), prices, currencyCode, 'font-size: 0.7em')
}

const start = () => {
  const $priceContainer = document.querySelector('#ProductPrice')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
