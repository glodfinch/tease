import { calculatePrices, renderListPrice, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const justGrams = /^(?<weight>\d+)g(?: cake| sample| loose leaf tea| pouch| vacuum-sealed pouch)?/i
const justKilos = /^(?<weight>\d+)kg(?: pouch)?/i
const tastingPack = /^tasting pack (?<qty>\d+)x(?<weight>\d+)g$/i

const parseWeightString = (string) => {
  string = string.trim()
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  match = justKilos.exec(string)?.groups
  if (match) return match.weight * 1000
  match = tastingPack.exec(string)?.groups
  if (match) return match.qty * match.weight
  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()

  const selectedWeightEl = document.querySelector('.omega .swatch_options input:checked')
  const weightValue = selectedWeightEl.getAttribute('value')
  if (!weightValue) return false
  const selectedWeight = parseWeightString(weightValue)

  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()
  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)
  const isSoldOut = document.querySelector('.omega .modal_price .sold_out').textContent === 'Sold Out'

  if (isSoldOut) renderListPrice(document.querySelector('.omega .modal_price .sold_out'), listPrice, currencyCode)

  if (selectedWeight) {
    renderPrices(
      document.querySelector('.omega .modal_price'),
      prices,
      currencyCode,
      'line-height: 1; flex-basis: 100%; font-size: 0.5em;'
    )
  }
}

const start = () => {
  const $priceContainer = document.querySelector('.omega .current_price')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
