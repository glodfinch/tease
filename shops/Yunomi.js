import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^(?<weight>\d+)g$/i
const kilograms = /^(?<weight>\d+)kg$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const $selectedWeight = document.querySelector('.option-selector__btns input:checked')
  if (!$selectedWeight) return false

  const weightValue = $selectedWeight.getAttribute('value')
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()
  const selectedWeight = findGramWords(weightValue)
  if (!selectedWeight) return false

  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode

  const prices = await calculatePrices(listPrice, selectedWeight)
  renderPrices(document.querySelector('.product-info__price'), prices, currencyCode, 'flex-basis: 100%;')
}

const start = () => {
  // stop annoying layout jumping
  let styles = document.createElement('style')
  styles.textContent = `
    .product-info__price { min-height: 67px; }
    .product-info__price > .price { visibility: visible !important; display: flex !important; }
  `
  document.head.appendChild(styles)

  const $priceContainer = document.querySelector('.price .price__current')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
