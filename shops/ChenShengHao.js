import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^(?<weight>\d+)g$/i
const kilograms = /^(?<weight>\d+)kg$/i
const multipleGrams = /^(?<qty>\d+)\*(?<weight>\d+)g$/i
const multipleGramsTitle = /^(?<qty>\d+) \* (?<weight>\d+)g$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000
  match = multipleGrams.exec(string)?.groups
  if (match) return match.weight * match.qty

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const parseTitleString = (string) => {
  let match = multipleGramsTitle.exec(string)?.groups
  if (match) return match.weight * match.qty
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const selectedWeightEl = document.querySelector('.swatch-items-wrapper .swatch-element input:checked')
  const weightValue = selectedWeightEl.getAttribute('value')
  const selectedProduct = products.filter((p) => p.title.toLowerCase() === weightValue.toLowerCase()).pop()

  let selectedWeight = parseTitleString(weightValue)
  if (!selectedWeight) selectedWeight = findGramWords(weightValue)
  if (!selectedWeight) return false

  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)

  const hasTeaseEl = document.getElementById('#teaseCont')

  if (!hasTeaseEl) {
    const $tease = document.createElement('div')
    $tease.id = 'teaseCont'

    const $pp = document.querySelector('#ProductPrice')
    $pp.parentNode.insertBefore($tease, $pp.nextSibling)
  }

  renderPrices(
    document.querySelector('#teaseCont'),
    prices,
    currencyCode,
    'display: inline-flex; font-size: 0.7em; font-weight: 500;'
  )
}

const start = () => {
  const $priceContainer = document.querySelector('#ProductPrice')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
