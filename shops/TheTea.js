import { parsePrice } from '../lib/parsePrice.js'
import { startMutationObserver } from '../lib/utils.js'
import { calculatePrices, renderPrices } from '../render.js'

const grams = /^(?<weight>\d+)(-\d+)?g$/i
const kilograms = /^(?<weight>\d+)kg$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (let i = 0; i < split.length; i++) {
    let word = split[i]
    const oneAhead = split[i + 1]
    if (oneAhead && oneAhead === 'g') word = `${word}g`
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const $weightDropdown = document.getElementById('pa_waga')
  if (!$weightDropdown) return false
  const weightValue = $weightDropdown.options[$weightDropdown.selectedIndex].textContent
  const selectedWeight = findGramWords(weightValue)
  if (!selectedWeight) return false

  const $price = document.querySelector('.single_variation_wrap .woocommerce-variation .woocommerce-Price-amount bdi')
  if (!$price) return false
  const priceText = $price.textContent

  const { value, currencyCode } = parsePrice(priceText)
  const prices = await calculatePrices(value, selectedWeight)

  const hasTeaseEl = document.getElementById('#teaseCont')

  if (!hasTeaseEl) {
    const $tease = document.createElement('div')
    $tease.id = 'teaseCont'
    const $pp = document.querySelector('.single_variation_wrap .woocommerce-variation .woocommerce-Price-amount')
    $pp.parentNode.insertBefore($tease, $pp.nextSibling)
  }

  renderPrices(document.querySelector('#teaseCont'), prices, currencyCode)
}

const start = () => {
  const $priceContainer = document.querySelector('.single_variation_wrap .woocommerce-variation')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
