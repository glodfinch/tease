import { parsePrice } from '../lib/parsePrice.js'
import { startMutationObserver } from '../lib/utils.js'
import { calculatePrices, renderPrices } from '../render.js'

const justGrams = /^(?<weight>\d+)g.*?/i

const parseWeightString = (string) => {
  string = string.trim()
  let match = justGrams.exec(string)?.groups
  if (match) return match.weight
  return null
}

const renderTitleWeight = (weight) => {
  const title = document.querySelector('.Product__Info .ProductMeta__Title')
  Array.from(title.querySelectorAll('.tease-yeeontitle')).forEach((el) => el.remove())

  const cont = document.createElement('span')
  cont.className = 'tease-yeeontitle'
  cont.style = 'font-size: 0.7em'
  cont.textContent = `(${weight}g)`

  title.appendChild(cont)
}

const updatePrice = async (el) => {
  const selectors = document.querySelectorAll('.Product__Info .ProductForm__OptionName')
  let selectedWeightEl
  let selectedWeight

  for (const selector of selectors) {
    if (['weight:', 'size:'].includes(selector.childNodes[0].textContent.toLowerCase().trim())) {
      selectedWeightEl = selector.querySelector('.ProductForm__SelectedValue')
      break
    }
  }

  if (!selectedWeightEl) {
    const productData = JSON.parse(document.querySelector('script[data-product-json]').textContent)
    if (!productData || productData.product.variants.length > 1) return false
    selectedWeight = productData.product.variants[0].weight
    renderTitleWeight(selectedWeight)
  } else {
    const weightString = selectedWeightEl.textContent
    selectedWeight = parseWeightString(weightString)
  }

  if (!selectedWeight) return false

  const priceString = el.querySelector('.Price').textContent
  const { value, currencyCode } = parsePrice(priceString)
  const prices = await calculatePrices(value, selectedWeight)

  renderPrices(
    document.querySelector('.Product__Info .ProductMeta'),
    prices,
    currencyCode,
    'font-family: Montserrat,sans-serif; color: #6a6a6a;'
  )
}

const start = () => {
  const $priceContainer = document.querySelector('.Product__Info .ProductMeta__PriceList')
  startMutationObserver($priceContainer, () => updatePrice($priceContainer))
  updatePrice($priceContainer)
}

export default start
