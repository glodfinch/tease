import { calculatePrices, renderPrices } from '../render.js'
import { parseShopifyData } from '../lib/shopify.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^\(?~?(?<weight>\d+(\.\d+)?)g\)?$/i
const kilograms = /^\(?~?(?<weight>\d+)kg\)?$/i
const multipleGrams = /^(?<qty>\d+)x(?<weight>\d+)g$/i

const parseWeightString = (string) => {
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000
  match = multipleGrams.exec(string)?.groups
  if (match) return match.weight * match.qty

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const products = parseShopifyData()
  const selectedWeightEl = document.querySelector('.product__info-container .product-form__input input:checked')

  const selectedProduct = selectedWeightEl
    ? products.filter((p) => p.title.toLowerCase() === selectedWeightEl.getAttribute('value').toLowerCase()).pop()
    : products[0]

  const titleParsed = findGramWords(selectedProduct.product.title)
  const weightStringParsed = findGramWords(selectedProduct.title)

  if (!titleParsed && !weightStringParsed) return false

  const selectedWeight = titleParsed || weightStringParsed
  const listPrice = selectedProduct.price.amount
  const currencyCode = selectedProduct.price.currencyCode
  const prices = await calculatePrices(listPrice, selectedWeight)

  const hasTeaseEl = document.getElementById('#teaseCont')

  if (!hasTeaseEl) {
    const $tease = document.createElement('div')
    $tease.id = 'teaseCont'
    $tease.style = 'margin-top: -2.1rem;' // disgusting
    const $pp = document.querySelector('.product__info-container > div:nth-child(2)')
    $pp.parentNode.insertBefore($tease, $pp.nextSibling)
  }

  renderPrices(document.querySelector('#teaseCont'), prices, currencyCode, 'color: rgb(37,37,37);')
}

const start = () => {
  const $priceContainer = document.querySelector('.product__info-wrapper .product__title + div')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
