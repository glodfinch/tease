import { calculatePrices, renderPrices } from '../render.js'
import { parsePrice } from '../lib/parsePrice.js'
import { startMutationObserver } from '../lib/utils.js'

const grams = /^(?<weight>\d+)g$/i
const kilograms = /^(?<weight>\d+)kg$/i

const parseWeightString = (string) => {
  string = string.split(' ')[0]
  let match = grams.exec(string)?.groups
  if (match) return match.weight
  match = kilograms.exec(string)?.groups
  if (match) return match.weight * 1000

  return null
}

const findGramWords = (string) => {
  const split = string.split(' ')

  for (const word of split) {
    const parsed = parseWeightString(word)
    if (parsed) return parseFloat(parsed)
  }

  return null
}

const updatePrice = async () => {
  const selectedWeightEl = document.querySelector('.variations_form .variations li.active a.enabled')
  if (!selectedWeightEl) return false

  const weightValue = findGramWords(selectedWeightEl.getAttribute('data-value'))
  if (!weightValue) return false

  const $price = document.querySelector('.single_variation_wrap .woocommerce-variation .woocommerce-Price-amount bdi')
  const $textNode = [...$price.childNodes].filter(($node) => $node.nodeType === Node.TEXT_NODE).pop()
  if (!$textNode) return false
  const priceText = $textNode.textContent
  const { value, currencyCode } = parsePrice(priceText)
  const prices = await calculatePrices(value, weightValue)

  const hasTeaseEl = document.getElementById('#teaseCont')

  if (!hasTeaseEl) {
    const $tease = document.createElement('div')
    $tease.id = 'teaseCont'
    const $pp = document.querySelector('.single_variation_wrap .woocommerce-variation .woocommerce-Price-amount')
    $pp.parentNode.insertBefore($tease, $pp.nextSibling)
  }

  renderPrices(
    document.querySelector('#teaseCont'),
    prices,
    currencyCode,
    'color: #717171; font-weight: 400; font-size: 0.45em;'
  )
}

const start = () => {
  const $priceContainer = document.querySelector('.single_variation_wrap .woocommerce-variation')
  startMutationObserver($priceContainer, updatePrice)
  updatePrice()
}

export default start
