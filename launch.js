(async () => {
  const src = chrome.runtime.getURL('tease.js')
  const contentMain = await import(src)
  contentMain.default()
})()