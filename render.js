import { getSetting } from './lib/settings.js'

export const calculatePrices = async (price, weight) => {
  const secondaryWeight = await getSetting('secondaryWeight')
  const perGram = price / weight
  const perCake = perGram * secondaryWeight
  return { perGram, perCake }
}

const makePriceEl = (id, price, qty, priceStyles = '') => {
  const cont = document.createElement('div')
  cont.style = 'margin-right: 5px;'

  const text = document.createElement('span')
  text.style = `font-size: 1.4em; font-weight: semi-bold;${priceStyles}`
  text.textContent = price
  text.id = id

  const amount = document.createElement('span')
  amount.style = 'font-size: 1em;'
  amount.textContent = `/${qty}`

  cont.appendChild(text)
  cont.appendChild(amount)
  return cont
}

export const removePrices = () => {
  const elements = Array.from(document.querySelectorAll('.tease'))

  for (const el of elements) {
    el.remove()
  }
}

const formatPrice = async (price, currency) => {
  const decimalPlaces = await getSetting('decimalPlaces')

  const numberFormat = new Intl.NumberFormat(undefined, {
    style: currency === null ? 'decimal' : 'currency',
    currency: currency === null ? undefined : currency,
    currencyDisplay: 'narrowSymbol',
    minimumFractionDigits: decimalPlaces,
    maximumFractionDigits: decimalPlaces
  })

  return numberFormat.format(price)
}

export const renderListPrice = async (el, listPrice, currencyCode) => {
  const cont = document.createElement('div')
  cont.className = 'tease'
  cont.textContent = await formatPrice(listPrice, currencyCode)
  cont.style = 'font-size: 0.8em; color: #777777;'
  el.appendChild(cont)
}

export const renderPrices = async (el, prices, currencyCode, contStyles = '', priceStyles = '') => {
  const $main = document.getElementById('teaseMain')
  const $primary = document.getElementById('teasePrimaryPrice')
  const $secondary = document.getElementById('teaseSecondaryPrice')

  if ($main && $primary && $secondary) {
    $primary.textContent = await formatPrice(prices.perGram, currencyCode)
    $secondary.textContent = await formatPrice(prices.perCake, currencyCode)
  } else {
    const secondaryWeight = await getSetting('secondaryWeight')
    const cont = document.createElement('div')
    cont.className = 'tease'
    cont.id = 'teaseMain'
    cont.style = `padding-top: 5px; display: flex; align-items: center; font-size: 10px;${contStyles}`

    const pricePerGram = makePriceEl(
      'teasePrimaryPrice',
      await formatPrice(prices.perGram, currencyCode),
      '1g',
      priceStyles
    )

    const pricePerCake = makePriceEl(
      'teaseSecondaryPrice',
      await formatPrice(prices.perCake, currencyCode),
      `${secondaryWeight}g`,
      priceStyles
    )

    const img = document.createElement('img')
    img.src = chrome.runtime.getURL('icons/icon.png')
    img.style = 'width: 20px; height: 20px; margin-right: 3px;'

    cont.appendChild(img)
    cont.appendChild(pricePerGram)
    cont.appendChild(pricePerCake)
    el.appendChild(cont)
  }
}
